import React, { Component } from "react";
import listGlass from "./dataGlasses.json";
import "./style.css";

export default class Model extends Component {
  state = {
    id: 1,
    price: 0,
    name: "",
    urlImg: "",
    desc: "",
  };

  renderListGlass = () => {
    return listGlass.map((e, i) => {
      return (
        <img
          key={i}
          src={e.url}
          className="col-3 my-1"
          onClick={() => {
            this.handlePick(e.id);
          }}
        />
      );
    });
  };

  handlePick = (id) => {
    let index = listGlass.findIndex((e) => {
      return e.id === id;
    });
    let glass = listGlass[index];
    this.setState({
      id: glass.id,
      price: glass.price,
      name: glass.name,
      urlImg: glass.url,
      desc: glass.desc,
    });
  };

  render() {
    return (
      <div className="container">
        <div className="row justify-content-center  my-4">
          <div className="col-3 wrapper">
            <img src="./img/glassesImage/model.jpg" alt="" className="model" />
            <img src={this.state.urlImg} alt="" className="tryGlass" />
          </div>
          <div className="col-3 text-left">
            <p className="text text-info">Name: {this.state.name}</p>
            <p className="text text-danger">Price: ${this.state.price}</p>
            <p>Description:{this.state.desc}</p>
          </div>
        </div>
        <div className="row">{this.renderListGlass()}</div>
      </div>
    );
  }
}
