import React, { Component } from "react";

export default class Header extends Component {
  render() {
    return (
      <h1 className="bg-dark text-center text-white py-3">
        TRY GLASSES APP ONLINE
      </h1>
    );
  }
}
