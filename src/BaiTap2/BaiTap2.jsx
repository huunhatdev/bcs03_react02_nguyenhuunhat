import React, { Component } from "react";
import Header from "./Header";
import Model from "./Model";

export default class BaiTap2 extends Component {
  render() {
    return (
      <div>
        <Header />
        <Model />
      </div>
    );
  }
}
